package generator;

import common.FileWithTransactions;

import java.util.ArrayList;
import java.util.List;

/**
 * Класс, для генерации входных данных на вход принимает префикс генерируемых файлов.
 * todo -> принимать массив паспортов как args[]
 */
public class GeneratorMain {
    public static void main(String[] args) {
        // инициализация массива паспортов
        List<String> passports = new ArrayList<String>() {{
            add("1111 222222");
            add("2222 333333");
            add("3333 444444");
            add("4444 555555");
            add("6666 777777");
            add("8888 999999");
        }};
        FileWithTransactions fileWithTransactions = new FileWithTransactions();
        // генерация файлов
        fileWithTransactions.generateFile("generated", passports);
    }
}
