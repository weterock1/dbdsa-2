package common;


import common.model.Citizen;
import common.model.Transaction;

import java.io.File;
import java.io.PrintWriter;
import java.util.List;


/**
 * Класс для непосредственной реализации генерации
 * Использует модели
 * @see Transaction
 * @see Citizen
 * В качестве генерируемых сущностей, выходной формат CSV с , разделителем.
 */
public class FileWithTransactions {

    // Генарация и запись 3 файлов
    public void generateFile(String fileName, List<String> passports) {
        // файл для транзакций по зарплате
        File csvOutputFile = new File(fileName + "_salary");
        // файл для транзакций по поездкам
        File csvOutputFile2 = new File(fileName + "_abroad");
        // файл для личных данных пользователей
        File csvOutputFile3 = new File(fileName + "_citizen");
        //Генерация 1
        try (PrintWriter pw = new PrintWriter(csvOutputFile)) {
            for (int i = 0; i < passports.size(); i++) {
                for (int j = 0; j < 12; j++) {
                    pw.println(Transaction.generateOneTransaction("1", passports.get(i), String.valueOf(j)).propAsString());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        // Генерация 2
        try (PrintWriter pw = new PrintWriter(csvOutputFile2)) {
            for (int i = 0; i < passports.size(); i++) {
                for (int j = 0; j < 12; j++) {
                    pw.println(Transaction.generateOneTransaction("2", passports.get(i), String.valueOf(j)).propAsString());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Генерация 3
        try (PrintWriter pw = new PrintWriter(csvOutputFile3)) {
            for (int i = 0; i < passports.size(); i++) {
                pw.println(Citizen.generateOneTransaction(passports.get(i)).propAsString());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
