package common.model;


import java.util.List;
import java.util.Random;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Transaction {

    private static Random random = new Random();
    public static final int salaryBound = 100000;
    public static final int minSalary = 15000;

    Transaction(String transactionId, String type, String passport, String month, String salary) {
        this.transactionId = transactionId;
        this.type = type;
        this.passport = passport;
        this.month = month;
        this.salaryOrTrips = salary;
    }

    // Can be replaced by enum 1 or 2 this case only
    private String transactionId;
    private String type;
    private String passport;
    private String month;
    private String salaryOrTrips;

    public String propAsString() {
        return Stream.of(
                transactionId, type, passport, month, salaryOrTrips
        ).map(this::escapeSpecialCharacters)
                .collect(Collectors.joining(","));
    }

    public static Transaction generateOneTransaction(String type, List<String> passports) {
        if (!type.equals("1") && !type.equals("2"))
            throw new UnsupportedOperationException("Cannot use this type parameter. Try 1 or 2 exactly.");
        return new Transaction(
                UUID.randomUUID().toString(),
                type,
                passports.get(random.nextInt(passports.size())),
                String.valueOf(1 + random.nextInt(12)),
                String.valueOf(
                        type.equals("1") ?
                                minSalary + random.nextInt(salaryBound) :
                                random.nextInt(10)
                )
        );
    }

    public static Transaction generateOneTransaction(String type, String passports) {
        if (!type.equals("1") && !type.equals("2"))
            throw new UnsupportedOperationException("Cannot use this type parameter. Try 1 or 2 exactly.");
        return new Transaction(
                UUID.randomUUID().toString(),
                type,
                passports,
                String.valueOf(1 + random.nextInt(12)),
                String.valueOf(
                        type.equals("1") ?
                                minSalary + random.nextInt(salaryBound) :
                                random.nextInt(10)
                )
        );
    }

    public static Transaction generateOneTransaction(String type, String passports, String month) {
        if (!type.equals("1") && !type.equals("2"))
            throw new UnsupportedOperationException("Cannot use this type parameter. Try 1 or 2 exactly.");
        return new Transaction(
                UUID.randomUUID().toString(),
                type,
                passports,
                month,
                String.valueOf(
                        type.equals("1") ?
                                minSalary + random.nextInt(salaryBound) :
                                random.nextInt(10)
                )
        );
    }

    private String escapeSpecialCharacters(String data) {
        String escapedData = data.replaceAll("\\R", " ");
        if (data.contains(",") || data.contains("\"") || data.contains("'")) {
            data = data.replace("\"", "\"\"");
            escapedData = "\"" + data + "\"";
        }
        return escapedData;
    }
}
