package common.model;

import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Citizen {

    private static Random random = new Random();
    private String ageCategory;
    private String passportNumber;

    private static final String[] ageCategoryArray = new String[]{"SMALL", "YOUNG", "MEDIUM", "ELDER", "OLD"};

    Citizen(String ageCategory, String passportNumber) {
        this.ageCategory = ageCategory;
        this.passportNumber = passportNumber;
    }


    public String propAsString() {
        return Stream.of(
                ageCategory, passportNumber
        ).map(this::escapeSpecialCharacters)
                .collect(Collectors.joining(","));
    }

    public static Citizen generateOneTransaction(List<String> passports) {
        return new Citizen(
                passports.get(random.nextInt(passports.size())),
                ageCategoryArray[random.nextInt(ageCategoryArray.length)]
        );
    }

    public static Citizen generateOneTransaction(String passports) {
        return new Citizen(
                passports,
                ageCategoryArray[random.nextInt(ageCategoryArray.length)]
        );
    }

    private String escapeSpecialCharacters(String data) {
        String escapedData = data.replaceAll("\\R", " ");
        if (data.contains(",") || data.contains("\"") || data.contains("'")) {
            data = data.replace("\"", "\"\"");
            escapedData = "\"" + data + "\"";
        }
        return escapedData;
    }

}
