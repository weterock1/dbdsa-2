#!/bin/sh

#For the usage u should have a large count of util
# tar, wget, java

# Download kafka + zookeeper
wget http://apache-mirror.rbc.ru/pub/apache/kafka/2.4.0/kafka_2.12-2.4.0.tgz

#Untar folder
tar -xzf kafka_2.12-2.4.0.tgz

#go to the home kafka dir
cd kafka_2.12-2.4.0

#start zookeeper
bin/zookeeper-server-start.sh config/zookeeper.properties
#Start kafka
bin/kafka-server-start.sh config/server.properties
#Create topic
bin/kafka-topics.sh --create --bootstrap-server localhost:9092 --replication-factor 1 --partitions 1 --topic test

#Download cassandra
http://mirror.linux-ia64.org/apache/cassandra/3.11.5/apache-cassandra-3.11.5-bin.tar.gz

#Untar folder
tar -xzf apache-cassandra-3.11.5-bin.tar.gz

#go to the home kafka dir
cd ../apache-cassandra-3.11.5

# Start cassandra server
bin/cassandra

# log
echo -n "Zookeeper, Kafka and Kassandra started!"