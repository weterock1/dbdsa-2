package worker;

import com.google.gson.Gson;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.streaming.Durations;
import org.apache.spark.streaming.api.java.JavaDStream;
import org.apache.spark.streaming.api.java.JavaInputDStream;
import org.apache.spark.streaming.api.java.JavaPairDStream;
import org.apache.spark.streaming.api.java.JavaStreamingContext;
import org.apache.spark.streaming.kafka010.ConsumerStrategies;
import org.apache.spark.streaming.kafka010.KafkaUtils;
import org.apache.spark.streaming.kafka010.LocationStrategies;
import scala.Tuple2;
import worker.model.AmountAndCount;
import worker.model.Stat;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import static com.datastax.spark.connector.japi.CassandraJavaUtil.javaFunctions;
import static com.datastax.spark.connector.japi.CassandraJavaUtil.mapToRow;

/**
 * Класс осуществляющий оснонвную работу. Работает в режиме до прерывания.
 * Использует в качестве источника данных Kafka, сохраняет после успешного процессинга и группировки Cassandra.
 */
public class ConsumerWork {

    public static final String FILENAME = "generated_citizen";

    public static void main(String[] args) {

        // Инициализация массива с ассоциацей между номером пасспорта и возрастной группой
        Map<String, String> associatedMap = null;
        if (args.length > 0) {
            associatedMap = initAssociatedMap(args[0]);
        } else {
            associatedMap = initAssociatedMap(FILENAME);
        }

        // Инициализация спарка
        SparkConf sparkConf = new SparkConf();
        sparkConf.setAppName("SalaryAndAbroadTrip");
        sparkConf.set("spark.cassandra.connection.host", "127.0.0.1").setMaster("local[2]").set("spark.executor.memory","1g");

        JavaStreamingContext streamingContext = new JavaStreamingContext(
                sparkConf, Durations.seconds(1));

        Map<String, Object> kafkaParams = new HashMap<>();
        kafkaParams.put("bootstrap.servers", "localhost:9092");
        kafkaParams.put("key.deserializer", StringDeserializer.class);
        kafkaParams.put("value.deserializer", StringDeserializer.class);
        kafkaParams.put("group.id", "use_a_separate_group_id_for_each_stream");
        kafkaParams.put("auto.offset.reset", "latest");
        kafkaParams.put("enable.auto.commit", false);
        Collection<String> topics = Arrays.asList("test");

        // Подписка на топик и преобразование в Stream
        JavaInputDStream<ConsumerRecord<String, String>> messages =
                KafkaUtils.createDirectStream(
                        streamingContext,
                        LocationStrategies.PreferConsistent(),
                        ConsumerStrategies.<String, String>Subscribe(topics, kafkaParams));


        // Вычленение записи
        JavaPairDStream<String, String> results = messages
                .mapToPair(
                        record -> {
                            System.out.println("RECORD " + record.key() + " AND " + record.value());
                            return new Tuple2<>(record.key(), record.value());
                        }
                );

        // Удаление null ключа
        JavaDStream<String> lines = results
                .map(
                        Tuple2::_2
                );
        JavaDStream<List<String>> words = lines
                .map(
                        x -> Arrays.asList(x.split(","))
                );

        Map<String, String> finalAssociatedMap = associatedMap;

        // Рассчёт бизнес логики Сериализация оставляет все в String
        JavaPairDStream<String, String> wordCounts = words
                .mapToPair(
                        s -> new Tuple2<>(finalAssociatedMap.get(s.get(2)) + s.get(1), new Gson().toJson(new AmountAndCount(Integer.parseInt(s.get(4)), 1)))
                ).reduceByKey(
                        (i1, i2) -> {
                            Gson gson = new Gson();
                            return gson.toJson(gson.fromJson(i1,AmountAndCount.class).add(gson.fromJson(i2, AmountAndCount.class)));
                        }
                );

        // Сохранение плюс рассчёт финального среденго для величин по группе
        wordCounts.foreachRDD(
                javaRdd -> {
                    Map<String, String> wordCountMap = javaRdd.collectAsMap();
                    Gson gson = new Gson();
                    for (String key : wordCountMap.keySet()) {
                        String type = key.substring(key.length()-1);
                        String category = key.substring(0, key.length()-1);
                        AmountAndCount amountAndCount = gson.fromJson(wordCountMap.get(key), AmountAndCount.class);
                        String value = String.valueOf(amountAndCount.getAmount() / amountAndCount.getCount());
                        List<Stat> wordList = Arrays.asList(new Stat(category, type, value));
                        JavaRDD<Stat> rdd = streamingContext.sparkContext().parallelize(wordList);
                        javaFunctions(rdd).writerBuilder(
                                "test", "stat", mapToRow(Stat.class)).saveToCassandra();
                    }
                }
        );

        // запуск приложения
        streamingContext.start();
        try {
            streamingContext.awaitTermination();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * Производит парсинг файла, создавая ключ значения между Номер паспорт - Категория возраста
     * @param fileName файл для получения массива ассоциаций
     * @return
     */
    private static Map<String, String> initAssociatedMap(String fileName) {
        File initialFile = new File(fileName);
        InputStream targetStream = null;
        try {
            targetStream = new FileInputStream(initialFile);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        Scanner scanner = new Scanner(targetStream).useDelimiter("(,|\n)");
        HashMap<String, String> map = new HashMap<>();

        while(scanner.hasNext()){
            map.put(scanner.next(), scanner.next());
        }
        return map;
    }
}
