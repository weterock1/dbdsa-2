package worker.model;

public class AmountAndCount {

    private Integer amount;
    private Integer count;

    public AmountAndCount() {
    }

    public AmountAndCount(Integer amount, Integer count) {
        this.amount = amount;
        this.count = count;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public AmountAndCount add(AmountAndCount andCount) {
        return new AmountAndCount(this.amount + andCount.amount, this.count + andCount.count);
    }
}
