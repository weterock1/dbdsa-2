package worker.model;

import java.io.Serializable;
import java.util.UUID;

public class Stat implements Serializable {

    private String id;
    private String agecategory;
    private String type;
    private String amount;

    public Stat(String ageCategory, String type, String amount) {
        this.id = UUID.randomUUID().toString();
        this.agecategory = ageCategory;
        this.type = type;
        this.amount = amount;
    }

    public Stat() {
    }


    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAgecategory() {
        return agecategory;
    }

    public void setAgecategory(String agecategory) {
        this.agecategory = agecategory;
    }
}
