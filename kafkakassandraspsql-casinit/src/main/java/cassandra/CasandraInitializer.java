package cassandra;

import com.datastax.driver.core.Session;

/**
 * Класс для установки коннектов к базе, создания скриптов и их исполнения
 */
public class CasandraInitializer {

    public static void main(String[] args) {

        // Создание KEYSPACE script
        StringBuilder sb =
                new StringBuilder("CREATE KEYSPACE IF NOT EXISTS ")
                        .append("test").append(" WITH replication = {")
                        .append("'class':'").append("SimpleStrategy")
                        .append("','replication_factor':").append(1)
                        .append("};");

        // Создание таблицы внутри созданного keyspace
        StringBuilder sb2 = new StringBuilder("CREATE TABLE IF NOT EXISTS ")
                .append("test.stat").append("(")
                .append("id uuid PRIMARY KEY, ")
                .append("ageCategory text,")
                .append("type text,")
                .append("amount text);");

        String query = sb.toString();
        CassandraConnector client = new CassandraConnector();
        client.connect("127.0.0.1", 9042);
        Session session = client.getSession();
        //session.execute(sb.toString());
        session.execute(sb2.toString());

    }
}
